package com.example.testdeeplink

import android.annotation.SuppressLint
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.TextView
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks
import com.facebook.applinks.AppLinkData
import androidx.core.app.ComponentActivity
import androidx.core.app.ComponentActivity.ExtraData
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T



class MainActivity : AppCompatActivity() {

    lateinit var testText : TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        testText = findViewById(R.id.test_text)

        setupFirebaseDynamicLinks()
        setupFacebookDefferedDeepLinks()
    }

    @SuppressLint("SetTextI18n")
    fun setupFirebaseDynamicLinks() {
        FirebaseDynamicLinks.getInstance()
            .getDynamicLink(intent)
            .addOnSuccessListener(this) { pendingDynamicLinkData ->
                // Get deep link from result (may be null if no link is found)
                Log.d("DYNAMIC", "setuo liatener")
                var deepLink: Uri? = null
                if (pendingDynamicLinkData != null) {
                    Log.d("DYNAMIC", "got dynamic link")
                    deepLink = pendingDynamicLinkData.link

                    val code = deepLink?.getQueryParameter("id")
                    if(deepLink != null)
                        testText.text = "Recieved dynamic Link Pog. CODE = $code"
                }
            }
            .addOnFailureListener(this) { e -> Log.w("DYNAMIC", "getDynamicLink:onFailure", e) }
    }

    @SuppressLint("SetTextI18n")
    fun setupFacebookDefferedDeepLinks() {
        AppLinkData.fetchDeferredAppLinkData(this) {
            // Process app link data
            Log.d("FACEBOOK" , "Setup facebook deep links listener")
            if(it != null) {
                Log.d("FACEBOOK", "Got object from deep links") // this only triggers if deferred
                val promoCode = it.promotionCode
                if (promoCode.isNotEmpty())
                    Log.d("FACEBOOK", "Got code: $promoCode")
            }
        }
    }
}
